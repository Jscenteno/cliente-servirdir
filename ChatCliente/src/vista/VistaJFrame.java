
package vista;

import controlador.ControladorCliente;
import java.awt.Color;


public class VistaJFrame extends javax.swing.JFrame implements IVista {
    ControladorCliente controlador;
  
    public VistaJFrame() {
    	getContentPane().setBackground(new Color(128, 128, 128));
    	setTitle("Cliente");
        initComponents();
    }

  
    @SuppressWarnings("unchecked")
   
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTextAreaTrasiego = new javax.swing.JTextArea();
        jLabelTextoAEnviar = new javax.swing.JLabel();
        jTextFieldTextoAEnviar = new javax.swing.JTextField();
        jButtonEnviar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTextAreaTrasiego.setBackground(new Color(255, 255, 255));
        jTextAreaTrasiego.setColumns(20);
        jTextAreaTrasiego.setRows(5);
        jScrollPane1.setViewportView(jTextAreaTrasiego);

        jLabelTextoAEnviar.setText("Texto a Enviar:");

        jButtonEnviar.setText("Enviar");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelTextoAEnviar)
                                .addGap(18, 18, 18)
                                .addComponent(jTextFieldTextoAEnviar))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 339, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonEnviar)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelTextoAEnviar)
                    .addComponent(jTextFieldTextoAEnviar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonEnviar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }
    private javax.swing.JButton jButtonEnviar;
    private javax.swing.JLabel jLabelTextoAEnviar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextAreaTrasiego;
    private javax.swing.JTextField jTextFieldTextoAEnviar;
    // End of variables declaration//GEN-END:variables

    @Override
    public void habilitarEnviar() {
        jButtonEnviar.setEnabled(true);
    }

    @Override
    public void deshabilitarEnviar() {
        jButtonEnviar.setEnabled(false);
    }

    @Override
    public void agnadirMensajeATrasiego(String mensaje) {
        jTextAreaTrasiego.append(mensaje + "\n");
    }

    @Override
    public void borrarTextoAEnviar() {
        jTextFieldTextoAEnviar.setText("");
    }

    @Override
    public void setControlador(ControladorCliente controlador) {
        this.controlador = controlador;
    }

    @Override
    public void hacerVisible() {
        setVisible(true);
    }
    
    public void inicializar(){
        jButtonEnviar.setActionCommand(ENVIAR);
        jButtonEnviar.addActionListener(controlador);
    }

    @Override
    public String getMensajeAEnviar() {
        return jTextFieldTextoAEnviar.getText();
    }

}
